using System;
using System.Collections.Generic;

namespace WordsFrequency
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            if (args.Length == 0)
            {
                Console.WriteLine("Nope. Wrong arguments.");
            }

            Dictionary<string, uint> freq = new Dictionary<string, uint>();
            string[] words;

            if (args.Length > 1)
            {
                string parametrs = "";
                foreach (string str in args)
                    parametrs += str;

                words = parametrs.Split(',');
            }
            else
            {
                words = args[0].Split(',');
            }

            foreach (string word in words)
            {
                if (!freq.ContainsKey(word))
                    freq.Add(word, 0);
                freq[word]++;
            }

            Array.Sort(words, (string x, string y) => { return -freq[x].CompareTo(freq[y]); });


			Console.WriteLine("{0}\t{1}", words[0], freq[words[0]]);
		}
	}
}
